1.3.3 / 2018-07-26
==================

  * Fixed [Issue #6](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/6), which prevented the correct production of the artifacts for more than one file per markup type (basically always)


1.3.2 / 2018-07-23
==================

  * Fixed [Issue #5](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/5), now it can be run on a project with no .md, no .asciidoc, or none of them (to avoid breaking the rest of the pipeline)

1.3.1 / 2018-07-19
==================

  * AsciiDoc images (both internal and external) are now converted, in addition to the markup, as per [Issue #4](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/4).

1.3.0 / 2018-07-16
==================

  * Added support for **AsciiDoc** syntax on Wiki pages (through AsciiDoctor-PDF and various highlighter gems) as per [Issue #3](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/3) of [Milestone #1](https://gitlab.com/Andrea.Ligios/Wiki2PDF/milestones/1). Images have still problems which will be targeted in [Issue #4](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/4).

1.2.1 / 2018-07-16
==================

  * Fixed issue [#2](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/2)


1.2.0 / 2018-06-05
==================

  * It's now triggered by **Wiki modification Webhooks** and by the `Run Pipeline` button
  * It resolves external images referenced with both http and https URLs as asked in [#1](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/1)
  * It consider only images now, not every URL:
    - `[]()`: NO
    - `![]()`: YES
  * It doesn't download the same image twice, nor it tries to create the same folder twice
  * It doesn't break with a false positive when `/upload` (or `/http`) is part of the name other than the URL, like in `![foo /uploads/bar](/uploads/123/foo.png)`
  * Generated PDFs are now separated from source files and temporary files
  * URL and SCHEME for self-hosted installation have been declared as variable and put at the top, to be easily adjustable
  * Other minor improvements
  * Added splash screen in logs
  
1.0.2 / 2018-04-16
==================

  * It doesn't break anymore when put in a project which doesn't have Wiki pages yet.

