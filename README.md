# Wiki2PDF

Version: **1.3.3** (always look for the latest version for bugfixes and new functionalities)

[![pipeline status](https://gitlab.com/Andrea.Ligios/Wiki2PDF/badges/master/pipeline.svg)](https://gitlab.com/Andrea.Ligios/Wiki2PDF/commits/master)
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)


> Turns GitLab's Wiki pages into PDF

![Sample](/uploads/281696f33beb3baf896b7349a8d392a8/Schermata_da_2018-06-06_00-03-55.png)

## Why?

GitLab is a very cool platform, it provides almost everything you need, 
but it lacks integration between the **Project Repository**, the **Wiki Repository** 
(yeah, Wiki resides in a separate repository), and the **/uploads** folder 
where files are automatically stored by the Wiki's `Attach a file` functionality.

While there's a lot of people asking about *writing TO Wiki from a GitLab-CI pipeline*, 
I guess there's also a lot of people (like I was) hoping to be able to **export Wiki pages as PDF**.

It can be very useful, especially when there's no internet connection, 
or when the repository is self-hosted and we want to read our Wiki pages 
from outside our private network.

## How to Use It

Simply copy the content of this `.gitlab-ci.yml` into your `.gitlab-ci.yml`, 
and the pipeline will convert all your Wiki pages to PDF (on Wiki modification *or* after pressing the `Run Pipeline` button).

Pay attention to the spaces before the commands, **YAML is space-sensitive**, it will break on a bad indentation.

Explaining how to configure your Continuous Integration is beyond the scope of this README but, for those who are new to it, 
to trigger the pipeline only when a Wiki page is modified, and not (for example) when code is pushed to the project, it's enough to:

1. Create a _My-Wiki-Trigger_ under `Settings` > `CI/CD` > `Pipeline triggers`
2. Copy its **webhook URL** replacing `REF_NAME` with your branch (**master**) and `TOKEN` with the trigger's Token
3. Go to `Settings` > `Integrations` and use that URL to create a **webhook** having only the `Wiki Page events` checked (and `enable SSL verification` if you're on **https**)

### How To Use It on a self-hosted GitLab

Like above, but replaces the two variables `GITLAB_DOMAIN` and `GITLAB_SCHEME` with your self-hosted domain name and scheme (eg `foo.bar` and `http` if your self-hosted GitLab listen at `http://foo.bar/`).

## Which Markup Languages are supported?

- **MarkDown** is transformed into PDF through `kramdown` and `Prawn`. **MarkDown** syntax have several different flavors, and GitLab uses **Redcarpet**, not **Kramdown** ([not yet](https://gitlab.com/gitlab-com/gitlab-docs/issues/50)), 
hence the output must be checked carefully since the source may need to be adjusted to adhere to the right output flavor.

- **AsciiDoc** is transformed into PDF through `asciidoctor-pdf` and `Prawn`. **AsciiDoc** is standard, it gets rendered the same everywhere, and hence is the suggested way to go.

## How does it work

1. It clones the Wiki repository, 
2. parses the Wiki pages for images (both on GitLab's wiki and on the web), 
3. recreates the folder structure and download the referenced files (altering the cloned source files when needed, for example replacing `http://foo.bar/image.png` with `/http/foo.bar/image.png`)
4. creates a new **/wikis** folder so that it stands out as an untracked folder (and gets downloaded), and move the files there.

I've created a sample Wiki page which you can see transformed in PDF in the downloadable artifacts.

To try it yourself, simply fork the repository, create a sample Wiki page (mine won't get forked, since Wiki resides in a separate repository) 
and trigger the pipeline to see it getting transformed into PDF by GitLab-CI.

In the downloadable artifacts, along with the PDFs, you'll find the original Wiki pages and the **/uploads** folder containing the referenced resources, 
so that if something goes wrong for a .md or .asciidoc page, you'll be able to try to convert it manually with other tools (eg. Atom).

I've also left the temporary files used to create the resource folders and download the files, for debugging purposes.

## Known Issues

1. Prawn is able to handle most of the images out there, but I got problems with **interlaced PNG**s. 
   The solution is to open them with GIMP (or whatever), resave them with the `Interlacing (Adam7)` option **unchecked**, then reupload them to the Wiki page.

## Disclaimer

I'm not an expert on all the technologies involved; 
I just had a need, and I filled it with this solution because, 
AFAIK, there are no out-of-the-box ways to do it.

It's surely perfectible, but it works, and it's better than nothing.

Any feedback is appreciated. Cheers.